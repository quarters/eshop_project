package com.sun.webmanage.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.commons.utils.ActionUtils;
import com.sun.webmanage.model.TbItemParam;
import com.sun.webmanage.service.TbItemParamService;

@Controller
public class TbItemParamController {

	@Resource
	private TbItemParamService tbItemParamService;
	
	private final static Logger LOGGER = LoggerFactory.getLogger(TbItemController.class);
	
	@ResponseBody
	@RequestMapping("/item/param/list")
	public EasyUIDataGrid list(Integer page,Integer rows){
		return tbItemParamService.listTbItemParamPage(page, rows);
	}
	
	@ResponseBody
	@RequestMapping("/item/param/delete")
	public Map<String,Object> delete(String ids){
		boolean status = false;
		try {
			status = tbItemParamService.deleteTbItemByPK(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return ActionUtils.ajaxFail("删除失败", e.getMessage());
		}
		if(status){
			return ActionUtils.ajaxSuccess("删除成功", "");
		}
		return ActionUtils.ajaxFail("删除失败", "系统错误");
	}
	
	
	@ResponseBody
	@RequestMapping("/item/param/query/itemcatid/{catId}")
	public Map<String,Object> countParam(@PathVariable long catId){
		TbItemParam tbItemParam = null;
		try {
			tbItemParam = tbItemParamService.loadTbItemParamByCatId(catId);
		} catch (Exception e) {
			e.printStackTrace();
			return ActionUtils.ajaxFail("查找失败", "提示：系统错误");
		}
		if(tbItemParam!=null){
			return ActionUtils.ajaxSuccess("", tbItemParam);
		}
		//该分类下没有类目信息才可以新增
		return ActionUtils.ajaxSuccess("", null);
	}
	
	@ResponseBody
	@RequestMapping("/item/param/save/{catId}")
	public Map<String,Object> saveParamInfo(@PathVariable long catId,String paramData){
		boolean status = false;
		try {
			status = tbItemParamService.saveTbItemParam(catId, paramData);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("saveParamInfo saveTbItemParam error===="+e.getMessage());
			return ActionUtils.ajaxFail("类目新增失败", "提示：系统错误");
		}
		if(!status){
			return ActionUtils.ajaxFail("类目新增失败", "提示：系统错误");
		}
		return ActionUtils.ajaxSuccess("新增成功", "");
	}
	
	
}
