package com.sun.webmanage.service;

import com.sun.commons.pojo.EasyUIDataGrid;

/**
 * 同步记录
 * @author Sunhongmin
 *
 */
public interface SolrLogService {

	EasyUIDataGrid listSolrLog(int pager,int rows);
}
