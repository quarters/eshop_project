package com.sun.redis.dao;

/**
 * @author 孙哈哈
 * 操作redis缓存
 */
public interface RedisDao {

	String getKey(String key);
	
	String setKey(String key,String value);
	
	boolean exists(String key);
	
	long del(String key_name);
	
	long del(String... keys);
	
	
	/**
	 * 设置key有效时间  单位秒
	 * @param key
	 * @param time_in_seconds
	 * @return
	 */
	long expire(String key,int time_in_seconds);
}
