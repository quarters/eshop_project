package com.sun.item.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.commons.pojo.TbItemCatCustom;
import com.sun.commons.utils.JsonUtils;
import com.sun.dubbo.webmanage.service.TbItemCatDubboService;
import com.sun.dubbo.webmanage.service.TbItemDescDubboService;
import com.sun.dubbo.webmanage.service.TbItemDubboService;
import com.sun.dubbo.webmanage.service.TbItemParamItemDubboService;
import com.sun.item.pojo.ItemParam;
import com.sun.item.pojo.ItemParamItem;
import com.sun.redis.dao.RedisDao;
import com.sun.webmanage.model.TbItem;
import com.sun.webmanage.model.TbItemCat;
import com.sun.webmanage.model.TbItemDesc;
import com.sun.webmanage.model.TbItemParamItem;

@Service("ItemService")
public class ItemServiceImpl implements ItemService{

	@Reference
	private TbItemDubboService tbItemDubboService;
	@Reference
	private TbItemDescDubboService tbItemDescDubboService;
	@Reference
	private TbItemParamItemDubboService tbItemParamItemDubboService;
	@Reference
	private TbItemCatDubboService tbItemCatDubboService;
	
	@Value("${item.cache.item}")
	private String itemKey;
	@Value("${item.cache.item.desc}")
	private String itemDescKey;
	@Value("${item.cache.item.param}")
	private String itemParamKey;
	
	@Resource
	private RedisDao redisDao;
	
	@Override
	public TbItem loadTbItemById(long id) {
		String itemPathKey = itemKey + id;
		if(redisDao.exists(itemPathKey)){
			TbItem tbItem = JsonUtils.jsonToPojo(redisDao.getKey(itemPathKey), TbItem.class);
			return tbItem;
		}else{
			TbItem tbItem = tbItemDubboService.selectTbItemByPK(id);
			redisDao.setKey(itemPathKey, JsonUtils.objectToJson(tbItem));
			return tbItem;
		}
	}
	@Override
	public TbItemDesc loadTbItemDescByItemId(long item_id) {
		String itemDescPathKey = itemDescKey + item_id;
		if(redisDao.exists(itemDescPathKey)){
			TbItemDesc tbItemDesc = JsonUtils.jsonToPojo(redisDao.getKey(itemDescPathKey), TbItemDesc.class);
			return tbItemDesc;
		}else{
			TbItemDesc tbItemDesc = tbItemDescDubboService.selectTbItemDescByItemId(item_id);
			redisDao.setKey(itemDescPathKey, JsonUtils.objectToJson(tbItemDesc));
			return tbItemDesc;
		}
	}
	@Override
	public TbItemParamItem loadTbItemParamItemByItemId(long item_id) {
		String itemParamPathKey = itemParamKey + item_id;
		if(redisDao.exists(itemParamPathKey)){
			TbItemParamItem tbItemDesc = JsonUtils.jsonToPojo(redisDao.getKey(itemParamPathKey), TbItemParamItem.class);
			return tbItemDesc;
		}else{
			TbItemParamItem tbItemParamItem = tbItemParamItemDubboService.selectTbItemParamItemByItemId(item_id);
			redisDao.setKey(itemParamPathKey, JsonUtils.objectToJson(tbItemParamItem));
			return tbItemParamItem;
		}
	}
	@Override
	public TbItemCatCustom linkCat(Long catid) {
		TbItemCat leaf = tbItemCatDubboService.loadTbItemCatByPK(catid);
		TbItemCatCustom custom = new TbItemCatCustom();
		try {
			BeanUtils.copyProperties(custom, leaf, true);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return tbItemCatDubboService.linkCat(custom);
	}
	@Override
	public String TbItemParamHtml(String paramJsonString) {
		List<ItemParamItem> listParam = JsonUtils.jsonToList(paramJsonString, ItemParamItem.class);
		StringBuilder paramHtml = new StringBuilder();
		paramHtml.append("<div class='item-table'>");
		for (ItemParamItem itemParamItem : listParam) {
			paramHtml.append("<div class='item-table-group'>");
			paramHtml.append("<h3>"+itemParamItem.getGroup()+"</h3>");
			paramHtml.append("<dl>");
			for(ItemParam kv:itemParamItem.getParams()){
				paramHtml.append("<dl class=\"clearfix\" style=\"margin:0\">");
				paramHtml.append("<dt>"+kv.getK()+"</dt>");
				paramHtml.append("<dd>"+kv.getV()+"</dd>");
				paramHtml.append("</dl>");
			}
			paramHtml.append("</dl>");
			paramHtml.append("</div>");
		}
		paramHtml.append("</div>");
		
		return paramHtml.toString();
	}
	
	
	
	
}
